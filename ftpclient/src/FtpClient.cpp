#include "FtpClient.h"

#include <iostream>
#include <fstream>
#include <istream>
#include <ostream>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/regex.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/thread/thread.hpp>

using boost::asio::ip::tcp;

FtpClient::FtpClient() :
	m_controlSocket(m_ioService)
	, m_requestStream(&m_requestBuffer)
	, m_dataSocket(m_ioService)
	, m_responceStream(&m_responceBuffer)
{
}

FtpClient::~FtpClient()
{
	closeSocket();
}

bool FtpClient::connectToServer(std::string address)
{
	tcp::resolver resolver{m_ioService};
	tcp::resolver::query query{address, "ftp"};
	tcp::resolver::iterator endpoint_iterator = resolver.resolve(query);
	boost::asio::connect(m_controlSocket, endpoint_iterator);
	std::cout << "Connected to: " << m_controlSocket.remote_endpoint().address().to_string() << std::endl;

	getControlSocketResponce();

	if (m_controlSocket.is_open())
	{
		sendRequest("USER root");
	}
	getControlSocketResponce();
	getControlSocketResponce();
	return m_controlSocket.is_open();
}

void FtpClient::sendRequest(std::string command)
{
	m_requestStream.flush();
	m_requestStream << command <<"\r\n";

	boost::asio::write(m_controlSocket, m_requestBuffer);

}

void FtpClient::getDataSocketResponse()
{
	try
	{
		boost::asio::streambuf dataBuf;
		boost::system::error_code error;
		std::istream dataStream{&dataBuf};
		while (boost::asio::read(m_dataSocket, dataBuf, boost::asio::transfer_at_least(1), error))
		{
			(std::cout << &dataBuf).clear();
		}
		if (error != boost::asio::error::eof)
			throw boost::system::system_error(error);
	}
	catch (std::exception& e)
	{
		std::cout << "Exception: " << e.what() << "\n";
	}
	m_dataSocket.shutdown(tcp::socket::shutdown_both);
	m_dataSocket.close();
}

bool FtpClient::readCommand(std::string commandLine)
{
	auto spaceIndex = commandLine.find(" ", 0);

	if (!m_dataSocket.is_open())
		toPassiveMode();

	std::string command = commandLine.substr(0, spaceIndex);
	if (command == "ls")
	{
		if (spaceIndex < commandLine.size())
		{
			ls(commandLine.substr(spaceIndex + 1, commandLine.size()-1));
		}
		else
		{
			ls();
		}
	}
	else if (command == "cd")
	{
		if (spaceIndex < commandLine.size())
		{
			cd(commandLine.substr(spaceIndex + 1, commandLine.size() - 1));
		}
		else
		{
			cd();
		}
	}
	else if (command == "pwd")
	{
		pwd();
	}
	else if (command == "upload")
	{


		if (spaceIndex < commandLine.size())
		{
			upload(commandLine.substr(spaceIndex + 1, commandLine.size() - 1));
		}
		else
		{
			upload();
		}
	}
	else if (command == "download")
	{
		if (spaceIndex < commandLine.size())
		{
			download(commandLine.substr(spaceIndex + 1, commandLine.size() - 1));
		}
		else
		{
			download();
		}
	}
	else
		std::cout << "invalid command\n";

	return 0;
}

void FtpClient::getControlSocketResponce()
{
	boost::asio::read_until(m_controlSocket, m_responceBuffer, "\n");
	(std::cout << &m_responceBuffer).clear();
}

bool FtpClient::toPassiveMode()
{
	sendRequest("PASV");
	dataConnection();
	std::cout <<"responce socket is opened " << m_dataSocket.is_open() <<'\n';
	return m_dataSocket.is_open();
}

bool FtpClient::dataConnection()
{
	std::string resultString;
	boost::asio::streambuf dataBuf;
	std::istream dataStream{&dataBuf};

	boost::asio::read_until(m_controlSocket, dataBuf, "\n");
	std::getline(dataStream, resultString);

	endPoint endPoint;

	boost::cmatch regRes;
	boost::regex reg(".+\\((\\d+,\\d+,\\d+,\\d+),(\\d+),(\\d+)\\).*");

	if (!boost::regex_match(resultString.c_str(), regRes, reg))
	{
		throw(std::runtime_error("failed regular expretion"));
	}

	std::string ip {regRes[1]};
	std::replace(ip.begin(), ip.end(), ',', '.');
	int port = boost::lexical_cast<unsigned short>(regRes[2]) * 256 + boost::lexical_cast<unsigned short>(regRes[3]);;
	std::cout << ip <<" " << port << std::endl;
	m_endPoint.port = port;
	m_endPoint.addr= ip;
	tcp::endpoint ep(boost::asio::ip::address::from_string(m_endPoint.addr),m_endPoint.port);
	m_dataSocket.connect(ep);

	return m_dataSocket.is_open();
}

bool FtpClient::ls(std::string directory)
{
	if (directory == "")
		sendRequest("LIST");
	else
	{
		sendRequest("LIST " + directory);
	}
	getControlSocketResponce();
	getDataSocketResponse();
	getControlSocketResponce();
	return 1;
}

bool FtpClient::cd(std::string directory)
{
	sendRequest("CWD " + directory);
	getControlSocketResponce();
	return 1;
}

bool FtpClient::pwd()
{
	sendRequest("PWD");
	getControlSocketResponce();
	return 1;
}

bool FtpClient::upload(std::string file)
{
	if (file == "")
	{
		std::cout << "input filename\n";
		return 0;
	}
	else
	{
		std::cout << "send file name: " << file << "|\n";
		std::ifstream ifs(file.c_str(), std::ios::binary);

		size_t lastSlash = file.find_last_of('/');
		if (lastSlash < file.size())
			file = file.substr(lastSlash + 1);

		sendRequest("TYPE I");
		getControlSocketResponce();
		sendRequest("STOR " + file);
		getControlSocketResponce();

		boost::asio::streambuf dataBuf;
		boost::system::error_code error;
		std::istream dataStream{&dataBuf};

		while (!ifs.eof())
		{
			ifs >> &dataBuf;
			boost::asio::write(m_dataSocket, dataBuf, error);
			dataStream.clear();
		}

		ifs.close();
		m_dataSocket.shutdown(tcp::socket::shutdown_both);
		m_dataSocket.close();

		getControlSocketResponce();

		return 1;
	}

}

bool FtpClient::download(std::string file)
{
	if (file == "")
	{
		std::cout << "input filename\n";
		return 0;
	}
	else
	{
		sendRequest("TYPE I");
		getControlSocketResponce();
		sendRequest("RETR " + file);
		getControlSocketResponce();

		boost::asio::streambuf dataBuf;
		boost::system::error_code error;
		std::istream dataStream{&dataBuf};

		size_t lastSlash = file.find_last_of('/');
		if (lastSlash < file.size())
			file = file.substr(lastSlash + 1);
		std::cout << "download file name: " << file << "|\n";
		std::ofstream ofs(file.c_str(), std::ios::binary);

		while (boost::asio::read(m_dataSocket, dataBuf, boost::asio::transfer_all(), error))
		{
			ofs << &dataBuf;
			dataStream.clear();
		}

		if (error != boost::asio::error::eof) {
			throw boost::system::system_error(error);
		}

		ofs.close();
		m_dataSocket.shutdown(tcp::socket::shutdown_both);
		m_dataSocket.close();

		getControlSocketResponce();
		return 1;
	}
}



void FtpClient::closeSocket()
{
	m_controlSocket.close();
	m_dataSocket.close();
}
