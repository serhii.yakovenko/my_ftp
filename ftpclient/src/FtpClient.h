#pragma once
#include <string>

#include <boost/asio.hpp>

class FtpClient  
{
public:
	FtpClient();
	~FtpClient();

	struct endPoint{
		std::string addr;
		int port;
	};

	bool connectToServer(std::string address);

	void sendRequest(std::string command);
	void getControlSocketResponce();

	bool dataConnection();
	void getDataSocketResponse();

	bool readCommand(std::string commandLine);
	
	bool toPassiveMode();
	bool ls(std::string directory = "");
	bool cd(std::string directory = "");
	bool pwd();
	bool upload(std::string file = "");
	bool download(std::string file = "");


private:
	boost::asio::io_service m_ioService;

	boost::asio::streambuf m_requestBuffer;
	boost::asio::ip::tcp::socket m_controlSocket;
	std::ostream m_requestStream;

	boost::asio::streambuf m_responceBuffer;
	boost::asio::ip::tcp::socket m_dataSocket;
	std::istream m_responceStream;
	endPoint m_endPoint;

private:
	void closeSocket();
};

