#include <cstdlib>
#include <cstring>
#include <iostream>
#include <boost/asio.hpp>

#include "FtpClient.h"

using boost::asio::ip::tcp;

int main(int argc, char* argv[])
{
	if (argc == 1)
	{
		std::cout << "input server address\n";
		return 0;
	}

	FtpClient ftpClient{};

	try
	{
		if (ftpClient.connectToServer(argv[1]))
		{
//			if (ftpClient.toPassiveMode())
//			{
////				ftpClient.ls();
////				ftpClient.cd("bin");
////				ftpClient.pwd();
////				ftpClient.toPassiveMode();
////				ftpClient.ls();
//			}
//			else
//			{
//				std::cout<<"connection to data socket is fail"<<std::endl;
//			}
		}
		else
		{
			std::cout<<"connection to server is failed"<<std::endl;
		}

		while (1)
		{
			std::cout << "ftp: ";
			std::string command;
			getline(std::cin, command);
			ftpClient.readCommand(command);
		}
	}
	catch (std::exception& e)
	{
		std::cerr << "Exception: " << e.what() << "\n";
	}

  return 0;
}
