cmake_minimum_required(VERSION 3.5)

project(ftpserver)

find_package(Boost 1.65.1 COMPONENTS system thread regex) 

if(Boost_FOUND)
    include_directories(${Boost_INCLUDE_DIRS}) 
    add_executable(ftpserver main.cpp) 
    target_link_libraries(ftpserver ${Boost_LIBRARIES})
endif()